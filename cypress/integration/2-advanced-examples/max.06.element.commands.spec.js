/// <reference types="cypress" />
describe('Element Commands Sute', function() {
    it('Test 01 - scrolling', function() {
        cy.visit('https://almowafir.com')
        cy.viewport(1400, 850)
        cy.wait(2000)
        cy.scrollTo(0,500) //scroll 500px down
        cy.wait(2000)
        cy.scrollTo('bottom') //scroll to the bottom of the window
        cy.wait(2000)
        cy.scrollTo('center')
        cy.wait(2000)
        cy.scrollTo('0','25%') //scroll 25% down
        cy.wait(2000)
        cy.get('#c757 > :nth-child(5) > .c-cta-wrap > .c-offline-base-wrap > .offline-btn').scrollIntoView() //Scrolls into the view
        cy.wait(2000)
        cy.scrollTo(0, -500) //scroll 500px up
    })

    it('Test 02 - checkboxes', function() {
        cy.visit('https://atidcollege.co.il/Xamples/ex_controllers.html')
        cy.viewport(1400, 850)
        cy.wait(2000)
        cy.get('#tool-0').check()
        cy.wait(1000)
        cy.get('#tool-1').check()
        cy.wait(1000)
        cy.get('#tool-0').check()
        cy.wait(1000)
        cy.get('[type="checkbox"]').uncheck()
        cy.wait(1000)
        cy.get('[type="checkbox"]').check(['QTP', 'Selenium_IDE'])
        cy.wait(1000)
        cy.get('#continents').select('Africa')
        cy.wait(1000)
        cy.get('#continents').select('namerica')
        


        
     
    })

    


})
