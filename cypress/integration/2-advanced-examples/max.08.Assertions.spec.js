describe('Assertions', function() {
    it('Test 01 - Implicit Assertions BMI calc (', function() {
        cy.visit('https://atidcollege.co.il/Xamples/bmi')
        cy.get('#calculate_data').should('exist')
        cy.get('.basic-grey > :nth-child(1) > :nth-child(1) > :nth-child(1) > td').should('have.text', 'BMI Caclulator')
        cy.get('#calculate_data').should('exist')
        cy.get('#calculate_data').should('have.value', 'Calculate BMI')
        cy.get('#reset_data').should('exist')
        cy.get('#reset_data').should('have.value', 'Reset')
        
        cy.get('#calculate_data').click()
        cy.get('#validation').should('be.visible')
        cy.get('#validation').should('include.text', 'Please Fill in')
        //cy.get('#validation').should('include.text', 'blablabla') //failed test
        cy.get('#reset_data').click()

        cy.get('#weight').type('71')
        cy.get('#hight').type('165')
       
        cy.get('#calculate_data').click()
        cy.get('#bmi_result').should('have.value', '26')
        cy.get('#bmi_means').should('include.value', 'overweight')


        
      

        
    })




})
